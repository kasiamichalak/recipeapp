package pl.casmic.recipeapp.services;

import pl.casmic.recipeapp.model.Recipe;

import java.util.Set;

public interface RecipeService {

    Set<Recipe> getRecipes();

}
