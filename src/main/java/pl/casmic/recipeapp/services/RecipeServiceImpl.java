package pl.casmic.recipeapp.services;

import org.springframework.stereotype.Service;
import pl.casmic.recipeapp.model.Recipe;
import pl.casmic.recipeapp.repositories.RecipeRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class RecipeServiceImpl implements RecipeService {

    public final RecipeRepository recipeRepository;

    public RecipeServiceImpl(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public Set<Recipe> getRecipes() {
        Set<Recipe> recipes = new HashSet<>();
        recipes.addAll((List<Recipe>)recipeRepository.findAll());
        return recipes;
    }
}
