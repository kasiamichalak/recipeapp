package pl.casmic.recipeapp.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.casmic.recipeapp.model.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {

}
