package pl.casmic.recipeapp.model;

public enum Difficulty {
    EASY,
    MODERATE,
    HARD
}
